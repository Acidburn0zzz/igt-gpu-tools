#!/bin/sh
DOCKERFILE=$1
NAME=$2
echo Hello $DOCKERFILE
CHECKSUM=$(sha1sum $DOCKERFILE | cut -d ' ' -f1)
echo $CHECKSUM
FULLNAME=$CI_REGISTRY/$CI_PROJECT_PATH/$NAME
TAGNAME=$FULLNAME:$CHECKSUM
#IMAGE_PRESENT=$(docker pull $TAGNAME); echo "$?"
docker pull $TAGNAME
IMAGE_PRESENT=$(echo "$?")
set -e
echo $IMAGE_PRESENT
if [  $IMAGE_PRESENT -eq 0 ]; then
	echo "Not building" 
	docker pull $TAGNAME
	docker tag $TAGNAME $FULLNAME
	docker tag $TAGNAME $NAME
else
	echo "Building"
	docker build -t $TAGNAME -t $FULLNAME -t $NAME -f $DOCKERFILE .
	docker push $TAGNAME
fi
docker push $FULLNAME
